package io.rajas.droidstarter;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.support.v4.app.NavUtils;

public class FrontPage extends Activity {
	
	String[] examples = new String[] {"Yamba"};
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        //Populate example list
        
        ListView exampleList = (ListView) findViewById(R.id.exerciseList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.example_list, examples);
        exampleList.setAdapter(adapter);
        
        exampleList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
				handleClick(view, position, id);
			}
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_front_page, menu);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void handleClick(View view, int position, long id) {
    	Intent intent = new Intent(this, TimelineActivity.class);
    	startActivity(intent);
    }
}